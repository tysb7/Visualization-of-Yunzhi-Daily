// content.js

function injectButton() {

    // 获取当前日期对象
    var currentDate = new Date();
    // 获取当前年份
    var currentYear = currentDate.getFullYear();
    // 获取当前月份
    var currentMonth = currentDate.getMonth() + 1;

    // 创建最外层的div元素
    var outerDiv = document.createElement('div');
    outerDiv.setAttribute('data-v-35169100', '');
    outerDiv.setAttribute('data-v-258fcb4c', '');
    outerDiv.classList.add('sub-block-wrap', 'value-added');

    // 创建第一个子div元素
    var subTitleWrapDiv = document.createElement('div');
    subTitleWrapDiv.setAttribute('data-v-35169100', '');
    subTitleWrapDiv.classList.add('sub-title-wrap');

    // 创建第二个子div元素
    var subTitle = document.createElement('div');
    subTitle.setAttribute('data-v-35169100', '');
    subTitle.setAttribute('data-test', 'value-added');
    subTitle.classList.add('sub-title', 'cf-flex-grid', 'has-sub-menu');

    // 创建图标span元素
    var iconSpan = document.createElement('span');
    iconSpan.setAttribute('data-v-35169100', '');
    iconSpan.classList.add('sub-title-icon', 'cf-icon', 'cf-flex-grid-none', 'flow-portal');
    iconSpan.style = 'color: #f59c25;';
    subTitle.appendChild(iconSpan);


    // 创建标题span元素
    var titleSpan = document.createElement('span');
    titleSpan.setAttribute('data-v-35169100', '');
    titleSpan.setAttribute('title', '日报可视化');
    titleSpan.classList.add('title');
    titleSpan.textContent = '日报可视化';
    titleSpan.style = 'color: #f59c25;font-size:blod';

    // 将标题span添加到subTitle中
    subTitle.appendChild(titleSpan);

    // 将subTitle添加到subTitleWrapDiv中
    subTitleWrapDiv.appendChild(subTitle);

    // 最后，将subTitleWrapDiv添加到outerDiv中
    outerDiv.appendChild(subTitleWrapDiv);



    // 定义按钮点击事件
    outerDiv.addEventListener("click", function () {
        //   alert("Button clicked!");
        var ticket = localStorage.getItem('ticket')
        let url = `http://rb.tangstudio.cn:8123/?ticket=${ticket}&year=${currentYear}&month=${currentMonth}`
        window.open(url, '_blank');
    });

    // 将按钮添加到页面的 body 元素中
    var list = document.getElementsByClassName('sub-list not-dark');
    if (list.length > 0) {
        list[0].appendChild(outerDiv);
    } else {
        // Retry after a short delay if the target element is not found yet
        setTimeout(injectButton, 100);
    }
}

// Wait for the DOM to be ready before injecting the button
document.addEventListener("DOMContentLoaded", injectButton);
